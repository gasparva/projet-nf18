# Projet NF18
Gestion d'une  clinique vétérinaire



## Membres du groupe :

Vasco  Gaspar<br/>
Juliette Tougma<br/>
Florian Hue<br/>
Evan Morin

## Contenu :
NDC <br/>
MCD <br/>
MLD <br/>
SQL <br/>
Python <br/>
NoSQL <br/>
R-JSON

## Répartition des tâches :

**Projet 1 (NDC + MCD v1) :** <br/>
    - NDC : Florian <br/>
    - MCD : Juliette, Vasco, Evan <br/>

Florian : 20%, Juliette : 30%, Vasco : 30%, Evan : 20% <br/><br/>


**Projet 2 (MCD v2 + MLD v1) :** <br/>
    - MCD : Juliette <br/>
    - MLD v1 : Evan, Vasco, Florian <br/>

Florian : 15%, Juliette : 35%, Vasco : 15%, Evan : 35% <br/><br/>


**Projet 3 (MLD v2 + SQL CREATE et INSERT) :**<br/>
    - MLD v2 : Evan <br/>
    - SQL : Vasco, Florian, Juliette <br/>

Florian : 25%, Juliette : 25%, Vasco : 25%, Evan : 25% <br/><br/>


**Projet 4 (Finalisation SQL, SELECT et GROUP BY, début Python) :** <br/> 
    - SQL : Juliette, Evan, Florian <br/>
    - début Python : Vasco  <br/>

Florian : 25%, Juliette : 20%, Vasco : 30%, Evan : 25% <br/><br/>


**Projet 5 (Application Python) :** <br/>

Florian : 35%, Juliette : 20%, Vasco : 20%, Evan : 25% <br/><br/>


**Projet 6 (NoSQL et R-JSON) :** <br/>

Florian : 30%, Juliette : 20%, Vasco : 30%, Evan : 20% <br/><br/>


**Total :**  <br/>
Florian : 25%, Juliette : 25%, Vasco : 25%, Evan : 25%

